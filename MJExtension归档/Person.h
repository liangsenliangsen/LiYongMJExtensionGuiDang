//
//  Person.h
//  MJExtension归档
//
//  Created by 梁森 on 2018/1/11.
//  Copyright © 2018年 ----. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
// <NSCoding, MJKeyValue>
@interface Person : NSObject

/*
 */

@property (nonatomic, copy) NSString * ID;
@property (nonatomic, copy) NSString * name;
@property (nonatomic, copy) NSString * sex;

@end
