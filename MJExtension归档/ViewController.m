//
//  ViewController.m
//  MJExtension归档
//
//  Created by 梁森 on 2018/1/11.
//  Copyright © 2018年 ----. All rights reserved.
//

#import "ViewController.h"
#import "Person.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSDictionary * dic = @{@"id":@"1",@"name":@"梁森", @"sex":@"男"};
    
//        Person * model0 = [[Person alloc] init];
//        model0 = [Person mj_objectWithKeyValues:dic];
    
    Person * model0 = [Person mj_objectWithKeyValues:dic];
    NSLog(@"ID:%@", model0.ID);
    NSString *file = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"product.data"];
    BOOL successOrNot = [NSKeyedArchiver archiveRootObject:model0 toFile:file];
    NSLog(@"归档成功与否：%d", successOrNot);
    
    //解档
    Person *model  = [NSKeyedUnarchiver unarchiveObjectWithFile:file];
    NSLog(@"ID:%@=====Name:%@====Sex:%@",model.ID, model.name, model.sex);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
