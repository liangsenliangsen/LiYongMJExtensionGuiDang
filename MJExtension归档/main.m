
//
//  main.m
//  MJExtension归档
//
//  Created by 梁森 on 2018/1/11.
//  Copyright © 2018年 ----. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
